<?php

include 'conn.php';

$sql = "SELECT atr_nombre,atr_area FROM `crm_atr_atributo` ";

try {
    $dbh = new PDO("mysql:host=$dbhost;dbname=$dbname", $dbuser, $dbpass);
    $dbh->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    $stmt = $dbh->prepare($sql);
    //$stmt->bindParam("id", $_POST['id']);
    $stmt->execute();
    
    $atributos = $stmt->fetchAll(PDO::FETCH_OBJ);
    $dbh = null;
    echo '{"items":' . json_encode($atributos) . '}';
} catch (PDOException $e) {
    echo '{"error":{"text":' . $e->getMessage() . '}}';
}
